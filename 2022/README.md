Archives to be deposited in this folder.

# Repository Description

This repository hosts the tester archives for
the International Competition on Software Testing (Test-Comp).

# Contributing

Participants of Test-Comp can file a merge request to this repository in order
to make their tester available for the competition execution,
to the Test-Comp jury for inspection, and for later replication of the results.

For Test-Comp 2022, the archives are stored as
`2022/<tester>.zip`, where `<tester>` is
the identifier for the tester,
i.e., there exists a file `<tester>.xml`
in the repository with the benchmark definitions:
https://gitlab.com/sosy-lab/test-comp/bench-defs/tree/main/benchmark-defs

Validators use a prefix `val_`.

By filing a pull/merge request to this repository, contributors confirm
that the license for the archive is compatible with
the requirements of Test-Comp, as outlined in the rules
https://test-comp.sosy-lab.org/2021/rules_2020-09-23.pdf
under section Competition Environment and Requirements / Tester.


